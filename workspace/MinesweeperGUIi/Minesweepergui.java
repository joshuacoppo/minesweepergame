package MinesweeperGUI;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Minesweepergui extends Application {

// This is creating the board and tile dimensions.
    private static final int SIZEOFTILE = 40;
    private static final int W = 800;
    private static final int H = 600;
// Number of tiles in each direction
    private static final int TILEX = W / SIZEOFTILE;
    private static final int TILEY = H / SIZEOFTILE;
// Defines a two dimensional grid
    private Tile[][] grid = new Tile[TILEX][TILEY];
    private Scene scene;

    private Parent createContent() {
        Pane root = new Pane();
        root.setPrefSize(W, H);
// Populates the grid with tiles
        for (int y = 0; y < TILEY; y++) {
            for (int x = 0; x < TILEX; x++) {
// Instead of a set number of tiles there is a 20 % chance of a tile being spawned in a new tile
                Tile tile = new Tile(x, y, Math.random() < 0.2);

                grid[x][y] = tile;
                root.getChildren().add(tile);
            }
        }

        for (int y = 0; y < TILEY; y++) {
            for (int x = 0; x < TILEX; x++) {
                Tile tile = grid[x][y];

                if (tile.hasBomb)
                    continue;
// This obtains the neighbouring tiles, and filters by which tiles have bombs and returns how many bombs there are as text
                long bombs = getNeighbors(tile).stream().filter(t -> t.hasBomb).count();

                if (bombs > 0)
                    tile.text.setText(String.valueOf(bombs));
            }
        }

        return root;
    }
// This is to find the bombs around a tile
    private List<Tile> getNeighbors(Tile tile) {
        List<Tile> neighbors = new ArrayList<>();


        int[] points = new int[] {
              -1, -1,
// For example the line above, the first -1 is left of the x axis, so -1 from a point,
//and the second coordinate is -1 on the y axis, so if you clicked a spot it would be top left of a point and check that coordinate
              -1, 0,
              -1, 1,
              0, -1,
              0, 1,
              1, -1,
              1, 0,
              1, 1
        };
// This loop checks through the above list and finds all the neighbours of a selected tile
        for (int i = 0; i < points.length; i++) {
            int dx = points[i];
            int dy = points[++i];

            int newX = tile.x + dx;
            int newY = tile.y + dy;
// This checks if the checked tile is a valid tile
            if (newX >= 0 && newX < TILEX
                    && newY >= 0 && newY < TILEY) {
                neighbors.add(grid[newX][newY]);
            }
        }

        return neighbors;
    }
// giving each tile coordinates and saying they can have a bomb
    private class Tile extends StackPane {
        private int x, y;
        private boolean hasBomb;
        private boolean isOpen = false;
//This is creating a border for the tiles and the size is negative to make room for the stroke
        private Rectangle border = new Rectangle(SIZEOFTILE - 2, SIZEOFTILE - 2);
        private Text text = new Text();

        public Tile(int x, int y, boolean hasBomb) {
            this.x = x;
            this.y = y;
            this.hasBomb = hasBomb;

            border.setStroke(Color.BLUE);
// if a bomb is in a tile put an x
            text.setFont(Font.font(18));
            text.setText(hasBomb ? "X" : "");
            text.setVisible(false);

            getChildren().addAll(border, text);

            setTranslateX(x * SIZEOFTILE);
            setTranslateY(y * SIZEOFTILE);
// opens tiles on mouseclick
            setOnMouseClicked(e -> open());
        }

        public void open() {
            if (isOpen)
                return;
// If a tile with a bomb is clicked this just states you lost and creates a new game
            if (hasBomb) {
               System.out.println("You clicked a mine!! You lose, game over..");
               scene.setRoot(createContent());
               return;
            }
// If a tile is opened the number needs to become visible
            isOpen = true;
            text.setVisible(true);
            border.setFill(null);
//If a tile is opened with no neighbouring tiles adjacent tiles with no bombs should be opened
            if (text.getText().isEmpty()) {
                getNeighbors(this).forEach(Tile::open);
            }
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        scene = new Scene(createContent());

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}